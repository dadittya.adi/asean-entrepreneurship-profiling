<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndicatorItemsTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indicator_items_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('indicator_items_id');
            $table->unsignedInteger('test_id');
            $table->boolean('answer')->default(0);
            $table->timestamps();

            $table->foreign('indicator_items_id')->references('id')->on('indicator_items');
            $table->foreign('test_id')->references('id')->on('tests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indicator_items_tests');
    }
}
